import json


def readJson():
    with open("data.json") as file_obj:
        names = json.load(file_obj)
        return names;