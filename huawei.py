import json
import time

import requests

#第一步
def createDo():
	url = f"https://ord01.vmall.com/order/pwm86t/createOrder.do"

	headers = {
	  'Connection': 'keep-alive',
	  'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Mobile Safari/537.36',
	  'Accept': '*/*',
	  'Sec-Fetch-Site': 'same-site',
	  'Sec-Fetch-Mode': 'no-cors',
	  'Sec-Fetch-Dest': 'script',
	  'Referer': f'https://mitem.lenovo.com.cn/product/{gcode}.html',
	  'Accept-Language': 'zh-CN,zh;q=0.9',
	  'Cookie': cookie
	}

	data = {
		'uid': '70086000037438194',
		'user': '王******',
		'name': '王******',
		'ts': 1600395534728,
		'valid': 1,
		'sign': 'CA3D35BA595DAAED1894BF263DB136F4A8461F806CF289109E5937E621F511BE',
		'ticket': '1ST-10136511-SeEsc9DbxRH0aHWn4y00LFc1e4yDFidodG-cas',
		'hasphone': 1,
		'hasmail': 0,
		'logintype': 2,
		'nowTime': '2020-9-18 10:20:9',
		'skuId': 10086175997878,
		'skuIds': 10086175997878,
		'activityId': 860120200909547,
		'queueSign': '27411ff3eae6f75c2d31c4bcfcf7b8fdd6c08a5fb35d96c9093910aae903f008e5feeadf4fb98abbd1eb15a235753ea54a21650c386f9da1f5ec44ad629f48fc11dce06c439aae0af53543351c1670977961ee8713d6e5095a0156149f240547d5c76eea112b9e058344a55ef78b462ed6e6b9baa2d00d',
		'isqueue': 2,
		't': 1600395613533
	}

	response = requests.request("POST", url, headers=headers,data =data)
	print(response.text)
	# data = response.json()
	# print(data)
	# if data.get("success"):
	# 	return data["data"]


# 第二步
def submit(orderSign):
	url = f"https://mbuy.vmall.com/order/create.json"
	headers = {
	  'Connection': 'keep-alive',
	  'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Mobile Safari/537.36',
	  'Accept': '*/*',
	  'Sec-Fetch-Site': 'same-site',
	  'Sec-Fetch-Mode': 'no-cors',
	  'Sec-Fetch-Dest': 'script',
	  'Referer': f'https://mitem.lenovo.com.cn/product/{gcode}.html',
	  'Accept-Language': 'zh-CN,zh;q=0.9',
	  'Cookie': cookie
	}

	data={

			'duid': 70086000037438194,
			'uid': 70086000037438194,
			'skuIds': 10086175997878,
			'quantity': 1,
			'diyPackCodeArr': '',
			'diyPackSkusArr': '',
			'orderSign': 'BF5ACEC01DD4B52E18BB5E25CAECB0FEEAF177167E4768B99D2FA6F9E9371AA7CA23D43F0B0F3AA4D4985F775FA47576E7F7C82AEC2546E110BDE38E78ADE051E96FA95C91FE402ABF05ABA9B0B3EACDAD3E8ED803A111252C37114177DFE2DD84BDD6A9554D0CD425934781B229CCCD6472D825159D29EEDBBFB5D59A9D5DC6A8933E79D20295B4F6',
			'activityId': 860120200909547,
			'streetId': 34687,
			'street': '西大街街道',
			'districtId': 5300,
			'district': '临川区',
			'cityId': 5299,
			'city': '抚州',
			'provinceId': 5285,
			'province': '江西',
			'consignee': '王忠宁',
			'address': '开心路22号2楼',
			'mobile': 15519447512,
			'zipCode': '',
			'custName': '王******',
			'titleType': '50',
			'invoiceTitle': '个人',
			'taxpayerIdentityNum':'',
			'orderSource': 3,
			'activityUid': 70086000037438194,
			'nickName': '王******',
	}

	response = requests.request("POST", url, headers=headers,data =data)
	data = response.json()
	print(data)
	if data.get("success"):
		return data["t"]



# 检验通过后的token 重新拼接出提交url
def get_buy_url(token_dict):
	token= token_dict["token"]
	tokentimestamp = token_dict["timestamp"]
	url = f"https://mbuy.lenovo.com.cn/checkout/lenovo.html?buytype=1&ss=612&shopId=1&terminal=2&token={token}&tokenTimeStamp={tokentimestamp}&defaultPayment=&isSeckill=1"
	print(url)

def main():
	if not cookie:
		print("没有设置好cookie， 请填写~~~")
		return

	while True:
		time.sleep(2)
		createDo()
		# code="BF5ACEC01DD4B52E18BB5E25CAECB0FEEAF177167E4768B99D2FA6F9E9371AA7CA23D43F0B0F3AA4D4985F775FA47576E7F7C82AEC2546E110BDE38E78ADE051E96FA95C91FE402ABF05ABA9B0B3EACDAD3E8ED803A111252C37114177DFE2DD84BDD6A9554D0CD425934781B229CCCD6472D825159D29EEDBBFB5D59A9D5DC6A8933E79D20295B4F6"
		# submit(code)


# 商品id
gcode = "1009475"
# gcode = "1009657"
cookie = 'deviceid=cb35a9207f88174aab78c7c75d1da2ff; TID=cb35a9207f88174aab78c7c75d1da2ff; euid=943bf7d1d44c2ebe86905926b53cd21dc7a1b57c2a106c5e; uid=70086000037438194; user=%E7%8E%8B******; name=%E7%8E%8B******; ts=1600395534728; valid=1; sign=CA3D35BA595DAAED1894BF263DB136F4A8461F806CF289109E5937E621F511BE; ticket=1ST-10136511-SeEsc9DbxRH0aHWn4y00LFc1e4yDFidodG-cas; hasphone=1; hasmail=0; logintype=2; rush_info="70086000037438194_1600395536_SEC:C7F78278D966A0E3C8D075554CC381C3A97C90DD457F4D0D34C74845B6CCD181"; rush_ext="70086000037438194_1_1_1600395536_SEC:F34C64A556DAE3EC0EF1F9C0DF2186B21C05A6A4A0C9344E0DDDE7086D1E92C8"; CSRF-TOKEN=43B16BF3CFF02455547997C22CF479B12A7EB7199082FB01; __ukmc=e4bf22912787cbd7ac81dd771d08de9f70086000037438194; isqueue-860120200909547-70086000037438194=2; cps_orderid=; hasLogin=1600395584652; HWWAFSESID=800127f887eca4fc64; HWWAFSESTIME=1600395589374; queueSign-860120200909547-70086000037438194=27411ff3eae6f75c2d31c4bcfcf7b8fdd6c08a5fb35d96c9093910aae903f008e5feeadf4fb98abbd1eb15a235753ea54a21650c386f9da1f5ec44ad629f48fc11dce06c439aae0af53543351c1670977961ee8713d6e5095a0156149f240547d5c76eea112b9e058344a55ef78b462ed6e6b9baa2d00d'
if __name__ == '__main__':
	main()