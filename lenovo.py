import requests


# 获取发票信息
def get_invoice():
    url = "https://buy.lenovo.com.cn/addCommonInvoice.jhtm?customername=%E4%B8%AA%E4%BA%BA&custType=0&invoiceType=1&ran=0.632595037853968"
    headers = {
        'Connection': 'keep-alive',
        'Accept': '*/*',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Mobile Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Dest': 'empty',
        'Referer': 'https://buy.lenovo.com.cn/',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cookie': cookie
    }

    response = requests.request("GET", url, headers=headers)
    data = response.json()
    if data.get("success"):
        print("invoiceId", data["data"]["id"])
        return data["data"]["id"]


# 获取收获地址id
def get_addres():
    url = "https://buy.lenovo.com.cn/consignee/getList.jhtm?type=SH&pageNum=1&pageSize=10&ran=0.7101026588196799"
    headers = {
        'Connection': 'keep-alive',
        'Accept': '*/*',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Mobile Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Dest': 'empty',
        'Referer': 'https://buy.lenovo.com.cn/',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cookie': cookie
    }

    response = requests.request("GET", url, headers=headers)
    data = response.json()
    if data.get("success"):
        for line in data["data"]:
            print("consigneeId: ", line["id"])
            print("*" * 40)
            print(line)
            print("*" * 40)


# 第一步
def get_token(gcode):
    url = f"https://sec.lenovo.com.cn/getToken.jhtm?ss=626&gcode={gcode}&_=1600392060683"
    # 初步检查 ss应该是无关紧要的随机数
    headers = {
        'Connection': 'keep-alive',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Mobile Safari/537.36',
        'Accept': '*/*',
        'Sec-Fetch-Site': 'same-site',
        'Sec-Fetch-Mode': 'no-cors',
        'Sec-Fetch-Dest': 'script',
        'Referer': f'https://mitem.lenovo.com.cn/product/{gcode}.html',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cookie': cookie
    }

    response = requests.request("GET", url, headers=headers)
    data = response.json()
    if data.get("success"):
        return data["data"]


# 第二步
def check_token(gcode, token_dict):
    token = token_dict["token"]
    tokentimestamp = token_dict["timestamp"]
    url = f"https://mqiang.lenovo.com.cn/api/seckill/seckill_qiang.jhtm?shopId=1&terminal=2&itemtype=0&gcodes={gcode}&icount=1&opgcode=&servicecode=&personalization=&sn=&token={token}&tokenTimeStamp={tokentimestamp}&_=1600392061093"
    headers = {
        'Connection': 'keep-alive',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Mobile Safari/537.36',
        'Accept': '*/*',
        'Sec-Fetch-Site': 'same-site',
        'Sec-Fetch-Mode': 'no-cors',
        'Sec-Fetch-Dest': 'script',
        'Referer': f'https://mitem.lenovo.com.cn/product/{gcode}.html',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cookie': cookie
    }
    response = requests.request("GET", url, headers=headers)
    data = response.json()
    if data.get("success"):
        return data["t"]


# 检验通过后的token 重新拼接出提交url
def get_buy_url(token_dict):
    token = token_dict["token"]
    tokentimestamp = token_dict["timestamp"]
    url = f"https://mbuy.lenovo.com.cn/checkout/lenovo.html?buytype=1&ss=612&shopId=1&terminal=2&token={token}&tokenTimeStamp={tokentimestamp}&defaultPayment=&isSeckill=1"
    print(url)


def submit(token_dict):
    # 进入到该环节后 无限重试提交 直到成功
    token = token_dict["token"]
    tokentimestamp = token_dict["timestamp"]
    url = "https://mbuy.lenovo.com.cn/qiangapi/api/seckill/submit.jhtm"
    payload = f"terminal=2&buyType=1&happyBeanNum=0&innerBuyMoney=0&consigneeId={consigneeId}&couponCode=&couponids=&cashCoupon=0&storeId=&storeNo=&token={token}&tokenTimeStamp={tokentimestamp}&paytype=0&deliverGoodsType=1&invoiceId={invoiceId}&invoiceType=0&invoiceheadType=0&invoiceheadcontent=%E4%B8%AA%E4%BA%BA&receiverPhone=&receiverEmail=&cmanagercode=&orderremark=&preSaleMobile=&checkoutType=normal&defaultPayment=&idDentity="
    headers = {
        'Connection': 'keep-alive',
        'Accept': '*/*',
        'X-Requested-With': 'XMLHttpRequest',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Mobile Safari/537.36',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'https://mbuy.lenovo.com.cn',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Dest': 'empty',
        'Referer': 'https://mbuy.lenovo.com.cn/',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cookie': cookie
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    data = response.json()
    if data.get("success"):
        print("提交成功~")
        return True
    else:
        print(data)
        return False


def main():
    if not cookie:
        print("没有设置好cookie， 请填写~~~")
        return
    while True:
        try:
            result_token_dict = get_token(gcode)
            check_token_dict = check_token(gcode, result_token_dict)

            # get_buy_url(check_token_dict)
            for _ in range(20):
                if submit(check_token_dict):
                    break
        except Exception as e:
            print(e)
        else:
            return


# 商品id
gcode = "1009657"

# 下面三项根据各自账号不同自行填写， 可以使用辅助查询接口~
cookie = '_gid=GA1.3.1316030950.1600391047; _ga=GA1.3.1766734084.1600391047; LA_F_T_10000001=1600391047311; LA_C_Id=_ck20091809040713178657433733567; LA_M_W_10000001=_ck20091809040713178657433733567%7C10000001%7C%7C%7C; leid=1.XSQbg+PN8RM; LA_R_T_10000001=1600391106068; cerpreg-passport="|2|1600391466|1602983466|bGVub3ZvSWQ6MTE6MTAwOTI0MjQyNzl8bG9naW5OYW1lOjExOjE1NTE5NDQ3NTEyfG1lbWJlcklkOjEwOjE2MDI5ODM0NjZ8Z3JvdXBDb2RlOjE6MXxpc0xlbm92bzoxOjA=|cErOxvUjK1/0D0rNOxM0/onN2hmgozJlTRvXtS28PzaABm6qb+MnWxRy60KP9BXW3LPW18SrXQDmVGDXKdlJ+LdVHfjx/EfLQW6K0BpcwWOGMVxo4A9bSZ6b3LZC/HrL66f2NdxZ0Pn/dwa0oKxqIDFPP87jnbj2VtX1kSzadvKRZzla45b6vILofUGLa93uXJmQfZlx4rZ98zl7zPmtaNEN9To46D3sWZxI/yuJ+s4siPZwMYLC5/ULuVzHuZV67h/ux3LXRX/Kgu1XVTXIee5QJiJc++mY2mb2JzAM8UuHOOUqQjaHyOzVyXnGYCgdWONCnrHmIYOVBdV4WxbErg==|"; LA_C_C_Id=_sk202009180104070.90243800.7478; s_cc=true; s_nr=1600394963627; s_sq=%5B%5BB%5D%5D; LA_V_T_N_S_10000001=1600398228652; LA_V_T_10000001=1600398232250; LA_F_T_10000231=1600398369191; LA_R_T_10000231=1600398369191; LA_V_T_10000231=1600398369191; LA_M_W_10000231=_ck20091809040713178657433733567%7C10000231%7C%7C%7C; _gat=1; LA_V_T_N_10000001=1600399781092; JSESSIONID=4A8F3E7A3202BE8CE0CD9AF2671E431A'
# 收货地址id
consigneeId = "4828038"
# 猜测发票抬头id
invoiceId = "40364"

### 辅助查询发票id， 地址id 当有多个地址时候，不要看错了。
"""
get_invoice()
get_addres()
"""

if __name__ == '__main__':
    main()