import json

import requests

class Showstart(object):
    def __init__(self):
        self.headers = {
            's':'4bf4820b64f391eb8f5653168525475a',
            'Content-Type': 'application/json',
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36 MicroMessenger/7.0.9.501 NetType/WIFI MiniProgramEnv/Windows WindowsWechat',
            'cookie':'o_s=https://wap.showstart.com/?type=2&cityId=0; Hm_lvt_da038bae565bb601b53cc9cb25cdca74=1603455574; Hm_lpvt_da038bae565bb601b53cc9cb25cdca74=1603455574; u_s=https://wap.showstart.com/pages/activity/detail/detail?activityId=121738',
            'sign':'75c3116697ce5d1bde01f65bf571fe9b',
            'st_flpv':'1603455573937iw9VhkzwN09a6o8A7d3M'
        }

    # post请求
    def postWithJson(self, url: object, data: object) -> object:
        res = requests.post(url, headers=self.headers, data=data)
        textToJson = res.json()
        print("当前请求的url为：{} 状态：{}".format(url, textToJson["state"]))
        return textToJson
if __name__ == '__main__':
    url ="https://wap.showstart.com/api/wap/activity/ticket/list.json"
    data = {
        'activityId':121738,
        'coupon':'',
        'st_flpv':'1603455573937iw9VhkzwN09a6o8A7d3M',
        'sign':'75c3116697ce5d1bde01f65bf571fe9b',
        'trackPath':'',
        'terminal':'wap'
    }
    headers = {
        'terminal':'wap',
        'Host':'wap.showstart.com',
        'Origin':'https://wap.showstart.com',
        'r':'1603542909',
        'Referer':'https://wap.showstart.com/pages/activity/detail/detail?activityId=121738',
        's': '4bf4820b64f391eb8f5653168525475a',
        'Content-Type': 'application/x-www-form-urlencoded',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36 MicroMessenger/7.0.9.501 NetType/WIFI MiniProgramEnv/Windows WindowsWechat',
        'cookie': 'o_s=https://wap.showstart.com/?type=2&cityId=0; Hm_lvt_da038bae565bb601b53cc9cb25cdca74=1603455574; Hm_lpvt_da038bae565bb601b53cc9cb25cdca74=1603455574; u_s=https://wap.showstart.com/pages/activity/detail/detail?activityId=121738',
        'sign': '75c3116697ce5d1bde01f65bf571fe9b',
        'st_flpv': '1603455573937iw9VhkzwN09a6o8A7d3M'
    }
    res = requests.post(url,data= json.dumps(data),headers =headers)
    print(res.json())