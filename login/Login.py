import os
import pickle
import time

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
def chrome_options(cls):
    mobile_emulation = {        "deviceMetrics": {"width":375, "height": 667, "pixelRatio": 2.0},        "userAgent":"Mozilla/5.0 (Linux; U; Android 4.0.4; en-gb; GT-I9300 Build/IMM76D) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30"        }
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_experimental_option("mobileEmulation", mobile_emulation)
    return chrome_options
path = "./chromedriver"# 注意这个路径需要时可执行路径（chmod 777 dir or 755 dir）
brower = webdriver.Chrome(executable_path=path)

# brower = webdriver.Chrome()
wait = WebDriverWait(brower, 10)


def getTaobaoCookies():
    # get login taobao cookies
    url = "https://passport.youzan.com/settings"
    brower.get("https://passport.youzan.com/login/password")

    while True:
        print("Please login in youzan.com!")
        time.sleep(3)
        # if login in successfully, url  jump to www.taobao.com
        while brower.current_url ==  url:
            tbCookies  = brower.get_cookies()
            brower.quit()
            cookies = {}
            for item in tbCookies:
                cookies[item['name']] = item['value']
            outputPath = open('taobaoCookies.pickle','wb')
            pickle.dump(cookies,outputPath)
            outputPath.close()
            return cookies
print(getTaobaoCookies())