import json
import time,datetime

import requests

headers = {
    'Content-Type': 'application/json',
    'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36 MicroMessenger/7.0.9.501 NetType/WIFI MiniProgramEnv/Windows WindowsWechat',
}



def post(url: object, data: object) -> object:
    res = requests.post(url, headers=headers, data=data)
    textToJson = res.json()
    print("当前请求的url为：{} 状态：{}".format(url,textToJson["code"]))
    if textToJson["code"]== 9001 or textToJson["code"]== 8001 :
        print(textToJson["message"])
    # if(not (textToJson["SUCCESS"])):
    #     pass
    # print(textToJson)
    return textToJson

# 第一步 找去用户的地址
def queryDefaultAddress():
    url = "https://crm.newbalance.com.cn/mall/address/queryDefaultAddress"
    data = {
        "sessionkey":sessionkey
    }
    res = post(url,json.dumps(data))
    success = res.get("success")
    if success:
        return res.get("data")
    return None




def orderSave():
    url="https://crm.newbalance.com.cn/mall/order/save"
    data={
        "product_id": sku_id,
        "qty":1,
        "address_id":address_id,
        "type":"1",
        "sessionkey":sessionkey
    }

    res = post(url, json.dumps(data))
    print(res)
    return  res;


def productInfo():
    url="https://crm.newbalance.com.cn/mall/center/productInfo";
    data ={
        "product_id": product_id,
        "sessionkey": sessionkey
    }
    try:
        res = post(url, json.dumps(data))
        sizeList =  res.get("data").get("detail")[0].get("sizeList")
        print("当前sku信息如下：\n")
        for sku in sizeList:
            # print(sku)
           print("颜色：{},尺码：{},规格值：{} ,库存：{}".format(sku.get("color"),sku.get("size"),sku.get("product_id") ,sku.get("stock")))
        return res
    except AttributeError:
        print("错误！")
        return None;

#获取配置
def getConfig(key):
    r = config.get("Basic", key)
    return r

def sendDing(message):
    url = "https://oapi.dingtalk.com/robot/send?access_token=dd6b8d21a46184a5d982d4e371cdf1a631b99b0d5368ad28cb355053577cd62b"
    headers = {
        'Content-Type': 'application/json',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Mobile Safari/537.36',
    }

    data = {
        "msgtype": "text",
        "text": {
            "content": "Newbalance通知：抢购成功！内容:{}".format(message)
        },
        "at": {
            "atMobiles": [
                "15519447512",
                "15085476476"
            ],
            "isAtAll": False
        }
    }

    send = requests.post(url, headers=headers, data=json.dumps(data))
    print(send.text)

def auth():
    headers = {
        'Content-Type': 'application/json',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Mobile Safari/537.36',
    }
    data = {
        "key": getConfig("auth"),
        "deviceId": "1"
    }
    res = requests.post("http://122.51.204.164:8080/auth", data=json.dumps(data), headers=headers)
    if (json.loads(res.text)["code"] == "200"):
        return True
    if (json.loads(res.text)["code"] == "400"):
        print("解析网站失败！")
    return False


def  checkSize(sizeList):
    for sku in sizeList:
        if sku.get("size") == size:
            if sku.get("stock") > 0.0:
                return sku.get("product_id")
    return None


if __name__ == '__main__':
    import configparser

    config = configparser.RawConfigParser()
    config.read("Newbalance.ini", encoding="utf-8")
    banner= """
                                          
                    DLLLLL   tLLLLLLLGG           
                    LGGGGG   LGGGLLLGLLG          
                                ,LLLGGGGi         
                   LGLGGGLD GGGGGj  .LGG;         
                  DGGGGGGLLLGGGGL   .GGL          
                           fGGGL.   LGLt          
                    ,fGGGLLLGGGLLLLGLGt           
                 LGGLLGGGGGGGGGLGLGGG             
                    .,GGGLLGLGG   GGGL            
                    :iLDGLLLLf    LGGGG           
               DLGLLGLLGGGGGGL   .LGGLE           
               DGGLLG.GGGGGGLLGGLGGGGG            
                       .  iLGGGGGGGLLD            
              GGGGLL   GGGGGLLLLLGLGt             
             GLLLLLD   GLLLLLLLLLLi               
                   .                              
                   G         G                    
   GLG  DL D  G D  fGL  :Gif G GG:jD;G  :Gf  DG   
   L L,Df.G G.L G  G .G L.jG GGL LfGD:D:L LELj f  
   L .GLGGL:LLG L  G  Li.  G GL.  fL  LL    LLGL  
   G .GG  , GL f   L  L f  G GD  DfG  LLD tfL: ,  
   L .G LGG  L G   EGL  GGGL L LG,LL. G DGL  GG;                            
    
    """
    print(banner)

    product_id = getConfig("product_id")
    sessionkey = input("请输入session_key:")
    # sessionkey = "2W+pK1gM4ivYMF7eJnc8Ow=="
    productInfoData = productInfo()
    address = queryDefaultAddress();
    address_id = address.get("address_id");
    sendResponse = address_id
    if address_id != None and productInfoData !=None and auth() == True :

        size = str(input("请复制尺码，然后回车确认："))
        # checkPromotionAndCoupon();
        # 日期
        startTime = datetime.datetime.strptime(getConfig("start_time"), '%Y-%m-%d %H:%M:%S')
        while datetime.datetime.now() < startTime:
            print('等待执行', datetime.datetime.now(), startTime)

        while True:
            productInfoRes = productInfo()
            sizeList = productInfoRes.get("data").get("detail")[0].get("sizeList")
            prid  = checkSize(sizeList)
            if prid !=None:
                sku_id =  prid
                break
            time.sleep(int(getConfig("stock_time")))


        while True:
            res = orderSave()

            if res.get("code") == 9010:
                time.sleep(int(getConfig("submit_time")))
            if res.get("code") == 8001:
                print("超时了！等待恢复吧。或者换个账号试试")
                time.sleep(10)
                break
            if res.get("code") == 1000:
                print("抢购成功！")
                sendDing(str(address))
                input("按任意键退出")
                break
    else:
        print("请填写默认地址！")