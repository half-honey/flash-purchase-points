import threading

import  requests
import json,re,time,datetime

from Iniconfigparser import confiGet, configSave


class ZuimeiMall():
    def __init__(self):
        self.shopId =re.findall('shop\d+',confiGet("producturl"),re.S)
        self.cookie = confiGet("cookie")
        self.KDTSESSIONID = re.search('KDTSESSIONID=(.*?);', self.cookie).group(1)
        self.startTime = confiGet("starttime")
        self.name = confiGet("name") #可选
        self.idCard =confiGet("idcard")
        self.address ={}
        self.alias =""
        self.bookKey=""
        self.kdt_id =""
        self.propsName =[]
        self.goodsId=""
        self.skuPrices =[]
        self.defaultSku ={}
        self.activityId =""
        self.activityType =""
        self.isPoint =0
        self.producturl = confiGet("producturl")
        self.isBuy =True
        self.ip ="124.160.213.129"
        self.headers = {
            'Content-Type': 'application/json',
            'User-Agent': 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Mobile Safari/537.36',
            'cookie': self.cookie
        }
    # def init(self):
    #     config = ZuimeiConfig()
    #     config.confiGet("price")
    # 解析商品
    def parseShop(self):

        # global  goodsId;
        # global  alias;
        # global kdt_id;
        # global skuId;
        # global price;
        # global defaultSku; # 积分sku  单独一个sku
        # global producturl;
        flag = True
        r = requests.get(self.producturl, headers=self.headers)
        data = r.text
        print("==================================================================================")
        data = re.findall('{"buyer_id".*false}', data,re.S)
        result = data[0]
        result = json.loads(result)


        self.kdt_id = result['goodsData']['shop']['kdtId']
        self.propsName = result['goodsData']['skuInfo'].get('props')
        self.goodsId = result['copy_info']['query']['goodsId']
        self.skuPrices = result['goodsData']['skuInfo']['skuPrices']
        self.defaultSku = result['goodsData']['skuInfo']['spuPrice']

        # self.alias = result['alias']
        self.alias = "1i1myi2at"
        print("goodsId->"+str(self.goodsId))
        print("kdt_id->"+str(self.kdt_id))
        print("alias->"+self.alias)
        print(self.propsName)
        print(self.skuPrices)
        print(self.defaultSku)
        total=int(confiGet("qianggousku"))

        print("==================================================================================")
        print(result)
        self.skuId = 36835469
        self.price = 149900
        # if self.propsName != []:
        #     for i in self.skuPrices:
        #         if i["price"] == total:
        #             self.skuId =i["skuId"]
        #             self.price =i["price"]
            # for i in range(len(self.propsName[0]["v"])):
            #     print("序号："+str(i)+"--->"+str(self.propsName[0]["v"][i]["name"]))
            #
            # while flag:
            #     try:
            #         nextVal = int(input("请选择sku："))
            #         self.propsName[0]["v"][nextVal]["id"]
            #         self.skuId = self.skuPrices[nextVal]['skuId']
            #         self.price = self.skuPrices[nextVal]['price']
            #         flag = False
            #     except IndexError:
            #         print("没有找到！")
            #     except ValueError:
            #         print("请输入数字！")
        # else:
        #
        #     self.skuId = self.defaultSku['skuId']
        #     self.price = self.defaultSku['price']

    #解析活动 获取积分活动信息
    def getActivity(self):
        headers = {
            'Content-Type': 'application/json',
            'User-Agent': 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Mobile Safari/537.36',
        }
        url = "https://" + self.shopId[0] + ".youzan.com/wscgoods/detail-api/user-goods-state.json"
        data = {
            'alias': self.alias,
            # 'banner_id':"f.71454254~cube.7~1~aubw62KP",
            # 'reft':"1597665543410_1597665556903",
            # 'spm':"f.48550879_f.71454254",
            'sf': "wx_menu",
            'goodsId': self.goodsId,
            'kdtId': self.kdt_id,
            'stockNum': 900,
            'supportExpress': True,
            'supportLocalDelivery': False,
            'supportSelfFetch': """alse""",
            'shopType': 0,
            'shopRole': 0,
            'pointsName': "积分",
            'hideShoppingCart': 0,
            'freightInsurance': 0,
            'isGift': 0,
            'isWebImInGoods': 1,
            'isWish': 1,
            'showBuyBtn': 1,
            'showShopBtn': 1,
            'showWscWebIm': 1,
            'soldOutRecommendSwitch': False,
            'isFastBuy': False,
            # "refHost": "shop46603518.m.youzan.com",
            # "refUrl": "https://shop46603518.m.youzan.com/wscgoods/detail/2xaenklp2ybqe?alias=2xaenklp2ybqe&kdtId=46411350",
            # # 'refHost': self.shopId[0] + '.m.youzan.com',
            # # 'refUrl': self.producturl,
            'lng': 0,
            'lat': 0
        }

        # TODO  获取不到活动的id
        r = requests.post(url, headers=headers,
                          data=json.dumps(data))
        result = json.loads(r.text)
        print(result)
        # print(result)
        print("当前状态：" + result["data"]["pageFeature"]["forbidBuyReason"])
        if result["data"]["marketing"]["activities"] != []:
            try:
                self.activityId = result["data"]["marketing"]["orderActivity"]["id"]
                self.activityType = result["data"]["marketing"]["orderActivity"]["code"]
                self.isPoint = '1'
                print(self.activityId)
            except KeyError:
                print("解析错误")



    def sumbitOrder(self):

        data = {
            'goodsList': [
                {
                    "goods_id": self.goodsId,
                    "num": int(confiGet("num")),
                    "sku_id": self.skuId,
                    "price": 0,
                    "dc_ps": "",
                    "qr": "",
                    "tpps": "",
                    "fcode": "",
                    "gdtClickId": None,
                    "message_0": self.name,
                    "message_1": self.idCard,
                    "isInstallment": False,
                    "propertyIds": [

                    ],
                    "isSevenDayUnconditionalReturn": False,
                    "atr_uuid": "",
                    "yzk_ex": "",
                    "page_type": "",
                    "points_price": float(self.price)/100
                }
            ],

            'common': {
                "kdt_id": self.kdt_id,
                "store_id": 0,
                "store_name": "",
                "postage": 0,
                "activity_alias": "",
                "activity_id": self.activityId,
                "activity_type": self.activityType,
                "use_wxpay": 0,
                "from": "",
                "order_from": "",
                "is_points": self.isPoint,
                "shopServiceKeys": [
                    "isSecuredTransactions",
                    "physicalStore",
                    "express"
                ],
                # "expressFeeStr":"运费：¥ 20.00"
            }
        }

        # print(json.dumps(data));
        r = requests.post('https://' + self.shopId[0] + '.youzan.com/wsctrade/order/goodsBook.json', headers=self.headers,
                          data=json.dumps(data), timeout=1)
        print("抢购成功！ 获取订单地址！")
        print(r.text)
        self.bookKey = json.loads(r.text)['data']['bookKey']
        self.parseOrder()

    def parseOrder(self):


        self.address;
        url = "https://cashier.youzan.com/pay/wsctrade_buy?book_key="+self.bookKey
        print("获取到订单地址：请复制到浏览器 打开！->"+url)
        headers = {
            'Content-Type': 'application/json',
            'User-Agent': 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Mobile Safari/537.36',
            'cookie':self.cookie,
            'Referer': 'https://ext-oder-info.isv.youzan.com/dynamic/index?id=data:time:1598155367442:c7bfc0c0'
        }
        # print(json.dumps(json.loads(data)))
        r = requests.get(url, headers=headers)
        print("解析订单！")
        print("==================================================================================")
        data = re.findall('{"buyer_id".*false}', r.text, re.S)
        result = data[0]
        print(data)
        result = json.loads(result)
        self.address=result["address"]["list"][0]
        print(result["address"]["list"][0])

    def confirmOrder(self):
        headers = {
            'Content-Type': 'application/json',
            'User-Agent': 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Mobile Safari/537.36',
            'cookie': self.cookie
        }

        data = {
            "version": 2,
            "source": {
                "bookKey": self.bookKey,
                "clientIp": self.ip,
                "fromThirdApp": False,
                "isWeapp": False,
                "itemSources": [{
                    "activityId": 0,
                    "activityType": 0,
                    "atrUuid": "",
                    # "bizTracePointExt": "{\"is_share\":\"1\",\"banner_id\":\"f.86215555~points_goods.7~2~RFFnjDm1\",\"st\":\"js\",\"sv\":\"0.8.19\",\"yai\":\"wsc_c\",\"uuid\":\"f817f753-f6b1-1341-625c-93e34da1a194\",\"platform\":\"h5\",\"biz\":\"wsc\",\"client\":\"\",\"atr_uuid\":\"\",\"yzk_ex\":\"\",\"page_type\":\"\"}",
                    "cartCreateTime": 0,
                    "cartUpdateTime": 0,
                    "gdtId": "",
                    "goodsId": self.goodsId,
                    "pageSource": "",
                    "propertyIds": [],
                    "skuId": self.skuId,
                    "yzkEx": ""
                }],
                "kdtSessionId": self.KDTSESSIONID,
                "needAppRedirect": False,
                "orderFrom": "",
                "orderType": 0,
                "platform": "unknown",
                "salesman": "",
                "userAgent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36",
                "bizPlatform": ""
            },
            "config": {
                "containsUnavailableItems": False,
                "fissionActivity": {
                    "fissionTicketNum": 0
                },
                "paymentExpiry": 0,
                "receiveMsg": True,
                "shopServiceKeys": ["isSecuredTransactions", "physicalStore", "express"],
                "usePoints": False,
                "useWxpay": False,
                "buyerMsg": "",
                "isForbidCoupon": False,
                "isForbidPreference": False
            },
            "usePayAsset": {},
            "items": [{
                "activityId": 0,
                "activityType": 0,
                "deliverTime": 0,
                "extensions": {
                    "OUTER_ITEM_ID": "10000"
                },
                "fCode": "",
                "goodsId": self.goodsId,
                "isInstallment": False,
                "isSevenDayUnconditionalReturn": False,
                "itemFissionTicketsNum": 0,
                "itemMessage": "[\"{}\",\"{}\"]".format(self.name, self.idCard),
                "kdtId": self.kdt_id,
                "num": int(confiGet("num")),
                "pointsPrice": self.price,
                "price": 0,
                "propertyIds": [],
                "skuId": self.skuId,
                "storeId": 0,
                "umpSkuId": 0
            }],
            "seller": {
                "kdtId": self.kdt_id,
                "storeId": 0
            },
            "ump": {
                "activities": [{
                    "activityId": 0,
                    "activityType": 0,
                    "externalPointId": 0,
                    "goodsId": self.goodsId,
                    "kdtId": self.kdt_id,
                    "pointsPrice": self.price,
                    "propertyIds": [],
                    "skuId": self.skuId,
                    "usePoints": False
                }],
                "coupon": {},
                "useCustomerCardInfo": {
                    "specified": False
                },
                "costPoints": {
                    "kdtId": self.kdt_id,
                    "usePointDeduction": True
                }
            },
            "unavailableItems": [],
            "delivery": {
                "hasFreightInsurance": False,
                "address": self.address,

                "expressType": "express",
                "expressTypeChoice": 0
            },
            "newCouponProcess": True,
            "isNorthAmericaShop": False,
            "asyncOrder": False,
            "cloudOrderExt": {
                "extension": {}
            },
            "confirmTotalPrice": self.price
        }

        # print(json.dumps(json.loads(data)))
        r = requests.post('https://cashier.youzan.com/pay/wsctrade/order/buy/v2/bill-fast.json', headers=headers,
                          data=json.dumps(data))
        print("订单提交成功！")
        text = json.loads(r.text)
        print(text)
        if text["code"]==0:
            self.sendDing(self.address.get("tel"))


            # pass

    def sendDing(self, tel):
        url = "https://oapi.dingtalk.com/robot/send?access_token=dd6b8d21a46184a5d982d4e371cdf1a631b99b0d5368ad28cb355053577cd62b"
        headers = {
            'Content-Type': 'application/json',
            'User-Agent': 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Mobile Safari/537.36',
        }

        data = {
            "msgtype": "text",
            "text": {
                "content": "通知：抢购成功！tel:{},{}".format(tel,self.getIp())
            },
            "at": {
                "atMobiles": [
                    "15519447512",
                    "189xxxx8325"
                ],
                "isAtAll": False
            }
        }

        send =requests.post(url, headers=headers, data=json.dumps(data))
        print(send.text)

    def getIp(self):
        a = requests.get("http://pv.sohu.com/cityjson")
        return a.text

    def  auth(self):
        headers = {
            'Content-Type': 'application/json',
            'User-Agent': 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Mobile Safari/537.36',
        }
        data = {
            "key": confiGet("auth"),
            "deviceId": "1"
        }
        res = requests.post("http://122.51.204.164:8080/auth", data=json.dumps(data), headers=headers)
        if (json.loads(res.text)["code"] == "200"):
            return True
        if (json.loads(res.text)["code"] == "400"):
            print("解析網站失敗！")
        return  False
    def getpointProduct(self):

        url ="https://shop46603518.youzan.com/wscshop/goods-api/goodsByTagAlias.json?pageSize=20&page=1&offlineId=0&openIndependentPrice=0&order=&json=1&alias=idjgme9z&needActivity=0"
        res= requests.get(url, timeout=2,headers= self.headers).text
        # res ="""
        #
        # {"code":0,"msg":"ok","data":{"hasMore":false,"list":[{"abilityMarkCodeList":[20001,30002,30003,10012,30007,10020,10022,10021],"activityPrice":"500.00","alias":"3emxcx91ggv1i","bizMarkCode":"000000000000","buyUrl":"","buyWay":1,"goodsType":0,"height":"800","id":669951070,"imageUrl":"https://img.yzcdn.cn/upload_files/2020/07/25/04439f879206d58196ed3ca146338c02.png","isVirtual":0,"origin":"","picture":[{"height":800,"id":2171478198,"url":"https://img.yzcdn.cn/upload_files/2020/07/25/04439f879206d58196ed3ca146338c02.png","width":800}],"postage":0,"preSale":false,"price":"500.00","sellPoint":"1元1分，购买后请点击确认收货，系统自动返送相应积分。","soldStatus":1,"startSoldTime":0,"subTitle":"1元1分，购买后请点击确认收货，系统自动返送相应积分。","title":"最美黔运积分卡（8.24.17）","totalSoldNum":0,"width":"800","url":"https://shop46603518.youzan.com/v2/goods/3emxcx91ggv1i"}]}}
        # """
        dataJson = json.loads(res)
        time.sleep(5)
        try:

            productUrl = dataJson["data"]["list"][0]["url"]
            self.sendDing(productUrl)
            t1 = threading.Thread(target=self.saveThread, args=(productUrl,))
            t1.start()

            return True
        except KeyError:
            print("时间： {} 商品不存在！----------------------->请等待上架！".format(datetime.datetime.now()))
            return  False
    def saveThread(self,productUrl):
        pass
        # configSave("producturl", productUrl)
# print(confiGet("currentgoods"))
# print(confiGet("price"))
a = ZuimeiMall();
print("开始解析！")
# while True:
#     if a.getpointProduct():
#         print("发现积分商品 ------------>延迟5秒后开始")
#         break;


for i in range(int(confiGet("thread"))):

    startTime = datetime.datetime.strptime(confiGet("starttime"), '%Y-%m-%d %H:%M:%S')

    if a.auth():
        while True:
            a.parseShop()
            # a.getActivity()
            try:
                a.sumbitOrder()
                while datetime.datetime.now() < startTime:
                    print('等待执行', datetime.datetime.now(), startTime)
                while True:
                    time.sleep(float(confiGet("interval")))
                    a.confirmOrder()
            except AttributeError:
                print("未找到相关sku")

    else:
        print("未授權")