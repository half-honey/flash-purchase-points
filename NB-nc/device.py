import wmi
import hashlib


class CpuMessage(object):
    def __init__(self):
        print("指纹信息如下：{}")

    def get_Cpu_Message(self):
        c = wmi.WMI()
        # CPU序列号
        for cpu in c.Win32_Processor():
            data = cpu.ProcessorId.strip()
            return data

    def encodeKey(self,key):
        return hashlib.md5(key.encode(encoding='UTF-8')).hexdigest()



cpu_message = CpuMessage()
print(cpu_message.encodeKey("BFEBFBFF000306C3"))