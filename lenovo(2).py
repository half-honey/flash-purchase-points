import requests
import time

# 下面三项根据各自账号不同自行填写， 可以使用辅助查询接口~
cookie = '_ga=GA1.3.1774723292.1600391240; _gid=GA1.3.1390309844.1600391240; LA_F_T_10000001=1600391240123; LA_C_Id=_ck20091809072011253073691019724; LA_R_T_10000001=1600391240123; LA_V_T_10000001=1600391240123; LA_M_W_10000001=_ck20091809072011253073691019724%7C10000001%7C%7C%7C; leid=1.ZNS0dj3NyTo; s_cc=true; s_sq=%5B%5BB%5D%5D; LA_V_T_N_S_10000001=1600391297708; s_nr=1600391302251; cerpreg-passport="|2|1600391388|1602983388|bGVub3ZvSWQ6MTE6MTAxODI0MjY0NDN8bG9naW5OYW1lOjExOjEzMjc5NDA0OTYzfG1lbWJlcklkOjEwOjE2MDI5ODMzODh8Z3JvdXBDb2RlOjE6MXxpc0xlbm92bzoxOjA=|KxZ2Pbd8dvJooHy0bD8WptnrZHBlVcJMlvl/JrVolVex/WEcyTXF7ivSDDyJZ7rcG6tu5JeTAPuM0XCcnnQInwgZrV51YUieXAc/nk0vcV5oP4RswFuLBBSijvS0y2DOnZeWvCFaSOoYMIwnVZ6nAA0FI14J+NDMa2ZwRr0dcse/1VNk0eT1JXuDaE2TdG7JBubzvvJGKN+9R2SdXQuccLFaJMeF3RrosHjI5KHd974Z1vCRLAmEc+IlxvPPMuqUDEoRF6WIqDyf3o4X33FMb8IE3/BjOiBdIJM4HBjTM+cIEmvSIhskdluWOu81iLdXH05Dsvmxdsl0ioz0TAMU5A==|"; LA_V_T_N_10000001=1600391996502; _gat=1'
# 收货地址id
consigneeId = "4850625" 
# 猜测发票抬头id
invoiceId= "40364"

### 辅助查询发票id， 地址id 当有多个地址时候，不要看错了。
"""
get_invoice()
get_addres()
"""
headers = {
	"GET": {
	  'Connection': 'keep-alive',
	  'Accept': '*/*',
	  'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Mobile Safari/537.36',
	  'X-Requested-With': 'XMLHttpRequest',
	  'Sec-Fetch-Site': 'same-origin',
	  'Sec-Fetch-Mode': 'cors',
	  'Sec-Fetch-Dest': 'empty',
	  'Referer': 'https://buy.lenovo.com.cn/',
	  'Accept-Language': 'zh-CN,zh;q=0.9',
	  'Cookie': cookie
	},
	"POST": {
	  'Connection': 'keep-alive',
	  'Accept': '*/*',
	  'X-Requested-With': 'XMLHttpRequest',
	  'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Mobile Safari/537.36',
	  'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
	  'Origin': 'https://mbuy.lenovo.com.cn',
	  'Sec-Fetch-Site': 'same-origin',
	  'Sec-Fetch-Mode': 'cors',
	  'Sec-Fetch-Dest': 'empty',
	  'Referer': 'https://mbuy.lenovo.com.cn/',
	  'Accept-Language': 'zh-CN,zh;q=0.9',
	  'Cookie': cookie
	}
}

def get_req(url, payload={},tag="GET"):
	if tag in ["GET", "POST"]:
		response = requests.request(tag, url, data=payload,  headers=headers[tag])
		data = response.json()
		if data.get("success"):
			return data
		print(data)
	return {}

# 获取发票信息
def get_invoice():
	url = "https://buy.lenovo.com.cn/addCommonInvoice.jhtm?customername=%E4%B8%AA%E4%BA%BA&custType=0&invoiceType=1&ran=0.632595037853968"
	data = get_req(url)
	if data.get("success"):
		print("invoiceId", data["data"]["id"])
		return data["data"]["id"]


# 获取收获地址id
def get_addres():
	url = "https://buy.lenovo.com.cn/consignee/getList.jhtm?type=SH&pageNum=1&pageSize=10&ran=0.7101026588196799"
	data = get_req(url)
	if data.get("success"):
		for line in data["data"]:
			print("consigneeId: ", line["id"])
			print("*"*40)
			print(line)
			print("*"*40)



#第一步
def get_token(gcode):
	url = f"https://sec.lenovo.com.cn/getToken.jhtm?ss=626&gcode={gcode}&_=1600392060683"
	# 初步检查 ss应该是无关紧要的随机数
	data = get_req(url)
	if data.get("success"):
		return data["data"]


# 第二步
def check_token(gcode, token_dict):
	if isinstance(token_dict, dict):
		token= token_dict["token"]
		tokentimestamp = token_dict["timestamp"]
		url = f"https://mqiang.lenovo.com.cn/api/seckill/seckill_qiang.jhtm?shopId=1&terminal=2&itemtype=0&gcodes={gcode}&icount=1&opgcode=&servicecode=&personalization=&sn=&token={token}&tokenTimeStamp={tokentimestamp}&_=1600392061093"
		data = get_req(url)
		if data.get("success"):
			return data["t"]



# 检验通过后的token 重新拼接出提交url
def get_buy_url(token_dict):
	if isinstance(token_dict, dict):
		token= token_dict["token"]
		tokentimestamp = token_dict["timestamp"]
		url = f"https://mbuy.lenovo.com.cn/checkout/lenovo.html?buytype=1&ss=612&shopId=1&terminal=2&token={token}&tokenTimeStamp={tokentimestamp}&defaultPayment=&isSeckill=1"
		print(url)

def submit(token_dict):
	if isinstance(token_dict, dict):
		# 进入到该环节后 无限重试提交 直到成功
		token = token_dict["token"]
		tokentimestamp = token_dict["timestamp"]
		url = "https://mbuy.lenovo.com.cn/qiangapi/api/seckill/submit.jhtm"
		payload = f"terminal=2&buyType=1&happyBeanNum=0&innerBuyMoney=0&consigneeId={consigneeId}&couponCode=&couponids=&cashCoupon=0&storeId=&storeNo=&token={token}&tokenTimeStamp={tokentimestamp}&paytype=0&deliverGoodsType=1&invoiceId={invoiceId}&invoiceType=0&invoiceheadType=0&invoiceheadcontent=%E4%B8%AA%E4%BA%BA&receiverPhone=&receiverEmail=&cmanagercode=&orderremark=&preSaleMobile=&checkoutType=normal&defaultPayment=&idDentity="
		data = get_req(url, payload=payload, tag="POST")
		if data.get("success"):
			print("提交成功~")
			return True
		else:
			print(data)
			return False

def main():
	if not cookie:
		print("没有设置好cookie， 请填写~~~")
		return
	while True:
		try:
			result_token_dict = get_token(gcode)
			check_token_dict = check_token(gcode, result_token_dict)
			# get_buy_url(check_token_dict)
			for _ in range(5):
				if submit(check_token_dict):
					return
		except Exception as e:
			print(e)




# 商品id
gcode = "1009657"

if __name__ == '__main__':
	main()