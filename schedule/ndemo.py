# -*- coding: utf-8 -*-
# Time: 2018/10/13 19:21:09
# File Name: ex_cron.py
import time
from datetime import datetime
import os
from apscheduler.schedulers.blocking import BlockingScheduler

def tick():
    print('Tick! The time is: %s' % datetime.now())

if __name__ == '__main__':
    scheduler = BlockingScheduler()
    scheduler.add_job(tick, 'cron', hour=22,minute='15',second=30,)

    print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C    '))
    while True:
        time.sleep(10)
        print("hello")
    try:
        scheduler.start()
    except (KeyboardInterrupt, SystemExit):
        pass
