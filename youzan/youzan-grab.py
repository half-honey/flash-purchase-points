import json
import threading
import time,datetime,re, configparser
import requests



def post(url: object, data: object) -> object:
    res = requests.post(url, headers = headers, data=data)
    textToJson = res.json()
    print("当前请求的url为：{} 状态：{}".format(url,textToJson["code"]))
    # if textToJson["code"]== 9001 or textToJson["code"]== 8001 :
    #     print(textToJson["message"])
    # if(not (textToJson["SUCCESS"])):
    #     pass
    # print(textToJson)
    return textToJson

def get(url: object) ->object:
    res = requests.get(url, headers=headers)
    textToJson = res.json()
    print("当前请求的url为：{} 状态：{}".format(url, textToJson["code"]))
    # if textToJson["code"]== 9001 or textToJson["code"]== 8001 :
    #     print(textToJson["message"])
    # if(not (textToJson["SUCCESS"])):
    #     pass
    # print(textToJson)
    return textToJson
#解析商品 ---
def parseShop(url):
    res = requests.get(url,headers = headers)
    data = re.findall('{"buyer_id".*false}', res.text, re.S)
    result = json.loads(data[0])
  #
    shopName = result.get("shopMetaInfo").get("shopName")
    # print(result.get("shopMetaInfo").get("kdtId"))
    address = result.get("mp_data").get("address")

    alias = result.get("goodsData").get("goods").get("alias")
    goods_id =result.get("goodsData").get("goods").get("id")
    title =result.get("goodsData").get("goods").get("title")
    kdtId = result.get("goodsData").get("goods").get("kdtId")
    type = result.get("goodsData").get("type")


    props = result.get("goodsData").get("skuInfo").get("props")
    prices = result.get("goodsData").get("skuInfo").get("skuPrices")

    # findProp(props,"s1")
    stock_list = result.get("goodsData").get("skuInfo").get("skuStocks")

    #展示基础信息
    print(
        "商店名称：{}\n商店地址：{}\n商品名称：{}\n其他信息：alias:{},goods_id:{} ,kdtIdL:{},type:{}"
        .format(shopName,address,title,alias,goods_id,kdtId,type)
    )


    print("*****************************************************************")
    if len(prices)==0:
        spuPrice = result.get("goodsData").get("skuInfo").get("spuPrice")
        spuStock = result.get("goodsData").get("skuInfo").get("spuStock")
        price = spuPrice.get("price")
        stockNum = spuStock.get("stockNum")
        print("当前默认规格为：价格：{},规格ID：{},库存：{}".format(spuPrice.get("price")/100,spuPrice.get("skuId"),stockNum))
        print("*****************************************************************")
    else:
        skus = result.get("goodsData").get("skuInfo").get("skus")
        showSkuInfo(props,prices,skus,stock_list)
        print("*****************************************************************")
    return {
        'alias':alias,
        'goods_id':goods_id,
        'price':0,
        'kdtId':kdtId
    }
def showSkuInfo(props,prices,skus,stock_list):
    sku_name_s1 = ""
    sku_name_s2 = ""
    sku_name_s3 = ""
    sku_name_s4 = ""
    sku_name_s5 = ""
    for sku in skus:
        s1 = sku.get("s1")
        s2 = sku.get("s2")
        s3 = sku.get("s3")
        s4 = sku.get("s4")
        s5 = sku.get("s5")
        skuId = sku.get("skuId")
        if(s1 != '0'):
            s1_data = findProp(props, "s1")
            res = findBySkuName(s1_data,s1)
            res.get("imgUrl")
            sku_name_s1 = res.get("name")
        if s2 !='0':
            s2_data = findProp(props, "s2")
            res=findBySkuName(s2_data, s2)
            sku_name_s2 = res.get("name")
        if s3 != '0':
            s3_data = findProp(props, "s3")
            res = findBySkuName(s3_data, s3)
            res.get("imgUrl")
            sku_name_s3= res.get("name")

        if s4 != '0':
            s4_data = findProp(props, "s4")
            res = findBySkuName(s4_data, s4)
            res.get("imgUrl")
            sku_name_s4 = res.get("name")
        if s5 != '0':
            s5_data = findProp(props, "s5")
            res = findBySkuName(s5_data, s5)
            res.get("imgUrl")
            sku_name_s5 = res.get("name")
        stock_info = findStockbySkuId(skuId,stock_list)
        price = findPricebySkuId(skuId,prices)
        print("规格:{} {} {} {} {}, 规格ID：{} ,库存信息：disable：{},库存：{} 价格：{}".format(sku_name_s1,sku_name_s2,sku_name_s3,sku_name_s4,sku_name_s5,skuId,
                                                           stock_info.get("disable"),stock_info.get("stockNum"),price/100
                                                           ))
        print("-----------------------------------------------------------------")

def findStockbySkuId(sku_id,data):
    for stock in data:
        disable =stock.get("disable")
        skuId =stock.get("skuId")
        stockNum =stock.get("stockNum")
        if sku_id == skuId:
            return {
                'disable':disable,
                'stockNum':stockNum
            }
    return None
def findPricebySkuId(sku_id,data):
    for price_e in data:
        if price_e.get("skuId") == sku_id:
            price = price_e.get("price")
            return price
    return None
def findBySkuName(datas,id):
    for data in datas:
        if data.get("id") == int(id):
            return data
    return None

def findProp(props,k_s):
    try:
        for prop in  props:
            if prop.get("k_s") == k_s:
                return  prop.get("v")
    except AttributeError:
        print("获取规格失败")
        return None;

def user_goods_state(goods_data):
    url = "https://shop"+getConfig("shop_id")+".youzan.com/wscgoods/detail-api/user-goods-state.json"
    # parseShop("https://shop44270126.m.youzan.com/wscgoods/detail/2xk9wjekru3hy")
    # goods_data = parseShop("https://shop44270126.m.youzan.com/wscgoods/detail/361qexetebz6e")

    data = {
            'alias': goods_data.get("alias"),
            'sf': "wx_menu",
            'goodsId': goods_data.get("goods_id"),
            'kdtId': goods_data.get("kdtId"),
            # 'stockNum': 900,
            'supportExpress': True,
            'supportLocalDelivery': False,
            'supportSelfFetch': """alse""",
            'shopType': 0,
            'shopRole': 0,
            'pointsName': "积分",
            'hideShoppingCart': 0,
            'freightInsurance': 0,
            'isGift': 0,
            'isWebImInGoods': 1,
            'isWish': 1,
            'showBuyBtn': 1,
            'showShopBtn': 1,
            'showWscWebIm': 1,
            'soldOutRecommendSwitch': False,
            'isFastBuy': False,
            'lng': 0,
            'lat': 0
        }
    res = post(url,json.dumps(data))
    # print(res)



#预创建订单提交信息 return：购买地址
def  preCreateOrder(goods_id,sku_id,kdt_id,buy_num):
    url ="https://shop"+getConfig("shop_id")+".youzan.com/wsctrade/order/goodsBook.json"
    data = {
        'goodsList': [
            {
                "goods_id": goods_id,
                "num": buy_num,
                "sku_id": sku_id,
                "price": 0,
                "dc_ps": "",
                "qr": "",
                "tpps": "",
                "fcode": "",
                "gdtClickId": None,
                #留言
                # "message_0": self.name,
                # "message_1": self.idCard,
                "isInstallment": False,
                "propertyIds": [

                ],
                "isSevenDayUnconditionalReturn": False,
                "atr_uuid": "",
                "yzk_ex": "",
                "page_type": "",
                # "points_price": float(self.price) / 100
            }
        ],

        'common': {
            "kdt_id": kdt_id,
            "store_id": 0,
            "store_name": "",
            "postage": 0,
            "activity_alias": "",
            "activity_id": 0,
            "activity_type": 0,
            "use_wxpay": 0,
            "from": "",
            "order_from": "",
            # "is_points": self.isPoint,
            "shopServiceKeys": [
                "isSecuredTransactions",
                "physicalStore",
                "express"
            ],
            # "expressFeeStr":"运费：¥ 20.00"
        }
    }
    res = post(url,data = json.dumps(data))
    print(res)
    byUrl = res.get("data").get("buyUrl")
    bookKey = res.get("data").get("bookKey")
    print("订单预提交地址：{}".format(byUrl) )
    return bookKey

def getDefaultSubmitMessage(bookKey):
    byUrl ="https://cashier.youzan.com/pay/wsctrade_buy?book_key="+bookKey
    # headers = {
    #     'Content-Type': 'application/json',
    #     'User-Agent': 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Mobile Safari/537.36',
    #     'Cookie': cookie,
    #     # 'Host': 'cashier.youzan.com',
    #     'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    #     # 'Referer': 'https://ext-oder-info.isv.youzan.com/dynamic/index?id=data:time:1598155367442:c7bfc0c0'
    # }
    r = requests.get(byUrl, headers = headers)
    print("================================正在解析订单！==================================================")
    data = re.findall("window._global = (.*?)\n", r.text, re.S)
    result = data[0]
    result = json.loads(result,encoding = "utf8")

    nickname = result.get("yz_user").get("nickname")
    mobile = result.get("yz_user").get("mobile")

    prepare = json.loads(result.get("prepare"))
    orderCreation = prepare.get("orderCreation")
    KDTSESSIONID= orderCreation.get("buyer").get("nobody")
    print("购买者姓名:{},购买者电话：{},KDTSESSIONID".format(nickname, mobile,KDTSESSIONID))
    address_dict =result.get("address").get("list")
    address = getDefaultAddress(address_dict)
    return {
        'nickname':nickname,
        'mobile':mobile,
        'KDTSESSIONID':KDTSESSIONID,
        'address':address,
        'address_dict':address_dict[0]
    }

def getDefaultAddress(data):
    for address_item in data:
        recipients = address_item.get("recipients")
        tel = address_item.get("tel")
        addressDetail = address_item.get("addressDetail")
        id = address_item.get("id")



        if address_item.get("isDefault") == 1:
            print("收件人：{},地址：{}，收件人电话：{}".format(recipients, addressDetail, tel))
            return {
                'recipients':recipients,
                'tel':tel,
                'addressDetail':addressDetail,
                'id':id
            }

def submit(bookKey,sku_id,goods_data,submit_data,buy_num):
    data = {
        "version": 2,
        "source": {
            "bookKey": bookKey,
            # "clientIp": '124.160.215.23',
            "fromThirdApp": False,
            "isWeapp": False,
            "itemSources": [{
                "activityId": 0,
                "activityType": 0,
                "atrUuid": "",
                # "bizTracePointExt": "{\"is_share\":\"1\",\"banner_id\":\"f.86215555~points_goods.7~2~RFFnjDm1\",\"st\":\"js\",\"sv\":\"0.8.19\",\"yai\":\"wsc_c\",\"uuid\":\"f817f753-f6b1-1341-625c-93e34da1a194\",\"platform\":\"h5\",\"biz\":\"wsc\",\"client\":\"\",\"atr_uuid\":\"\",\"yzk_ex\":\"\",\"page_type\":\"\"}",
                "cartCreateTime": 0,
                "cartUpdateTime": 0,
                "gdtId": "",
                "goodsId": goods_data.get("goods_id"),
                "pageSource": "",
                "propertyIds": [],
                "skuId": sku_id,
                "yzkEx": ""
            }],
            "kdtSessionId": submit_data.get("KDTSESSIONID"),
            "needAppRedirect": False,
            "orderFrom": "",
            "orderType": 0,
            "platform": "unknown",
            "salesman": "",
            "userAgent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36",
            "bizPlatform": ""
        },
        "config": {
            "containsUnavailableItems": False,
            "fissionActivity": {
                "fissionTicketNum": 0
            },
            "paymentExpiry": 0,
            "receiveMsg": True,
            "shopServiceKeys": ["isSecuredTransactions", "physicalStore", "express"],
            "usePoints": False,
            "useWxpay": False,
            "buyerMsg": "",
            "isForbidCoupon": False,
            "isForbidPreference": False
        },
        "usePayAsset": {},
        "items": [{
            "activityId": 0,
            "activityType": 0,
            "deliverTime": 0,
            "extensions": {
                "OUTER_ITEM_ID": "10000"
            },
            "fCode": "",
            "goodsId": goods_data.get("goods_id"),
            "isInstallment": False,
            "isSevenDayUnconditionalReturn": False,
            "itemFissionTicketsNum": 0,
            # "itemMessage": "[\"{}\",\"{}\"]".format("", ""),
            "kdtId": goods_data.get("kdtId"),
            "num": buy_num,
            "pointsPrice": 0,
            "price": 0,
            "propertyIds": [],
            "skuId": sku_id,
            "storeId": 0,
            "umpSkuId": 0
        }],
        "seller": {
            "kdtId": goods_data.get("kdtId"),
            "storeId": 0
        },
        "ump": {
            "activities": [{
                "activityId": 0,
                "activityType": 0,
                "externalPointId": 0,
                "goodsId": goods_data.get("goods_id"),
                "kdtId": goods_data.get("kdtId"),
                "pointsPrice": 0,
                "propertyIds": [],
                "skuId": sku_id,
                "usePoints": False
            }],
            "coupon": {},
            "useCustomerCardInfo": {
                "specified": False
            },
            "costPoints": {
                "kdtId": goods_data.get("kdtId"),
                "usePointDeduction": True
            }
        },
        "unavailableItems": [],
        "delivery": {
            "hasFreightInsurance": False,
            "address": submit_data.get("address_dict"),

            "expressType": "express",
            "expressTypeChoice": 0
        },
        "newCouponProcess": True,
        "isNorthAmericaShop": False,
        "asyncOrder": False,
        "cloudOrderExt": {
            "extension": {}
        },
        "confirmTotalPrice": 0
    }
    url="https://cashier.youzan.com/pay/wsctrade/order/buy/v2/bill-fast.json"
    # r= post(url,json.dumps(data))

    # headers = {
    #     'Content-Type': 'application/json',
    #     'User-Agent': 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Mobile Safari/537.36',
    #     'Cookie': cookie,
    #     # 'Host': 'cashier.youzan.com',
    #     'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    #     # 'Referer': 'https://ext-oder-info.isv.youzan.com/dynamic/index?id=data:time:1598155367442:c7bfc0c0'
    # }
    data = requests.post(url, headers=headers,
                      data=json.dumps(data)).json()
    if data.get("code") == 0:
        orderNo = data.get("data").get("orderNo")

        partnerReturnUrl = data.get("data").get("paymentPreparation").get("partnerReturnUrl")
        tradeDesc = data.get("data").get("paymentPreparation").get("tradeDesc")
        # nickname
        # ':nickname,
        # 'mobile': mobile,
        # 'KDTSESSIONID': KDTSESSIONID,
        # 'address': address,
        # 'address_dict': address_dict[0]
        msg = "[南川]抢购成功：订单编号：{} 购买者:{},手机：{}，地址:{} \n查询地址：{},描述：{}，商品地址：{}".format(orderNo,
                                                                       submit_data.get("nickname"),
                                                                       submit_data.get("mobile"),
                                                                       submit_data.get("address"),
                                                                       partnerReturnUrl,tradeDesc,  getConfig("goods_url")),


        sendDing(msg)
    else:
        print(data)
    return data
def sendDing(message):
    url = "https://oapi.dingtalk.com/robot/send?access_token=dd6b8d21a46184a5d982d4e371cdf1a631b99b0d5368ad28cb355053577cd62b"
    headers = {
        'Content-Type': 'application/json',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Mobile Safari/537.36',
    }

    data = {
        "msgtype": "text",
        "text": {
            "content": "有赞抢购通知：抢购成功！内容:{}".format(message)
        },
        "at": {
            "atMobiles": [
                "15519447512",
                "15085476476"
            ],
            "isAtAll": False
        }
    }

    send = requests.post(url, headers=headers, data=json.dumps(data))
    print(send.text)


#获取配置
def getConfig(key):
    r = config.get("Basic", key)
    return r
def main():



    url=getConfig("goods_url")
    goods_data = parseShop(url)
    user_goods_state(goods_data)
    sku_id = int(input("请输入规格ID:"))
    buy_num = int(input("请输入数量注意限购:"))
    # startTime = datetime.datetime.strptime(getConfig("start_time"), '%Y-%m-%d %H:%M:%S')
    # while datetime.datetime.now() < startTime:
    #     # time.sleep(0.1)
    #     print('等待执行', datetime.datetime.now(), startTime)
    # while True:
    #     bookKey = preCreateOrder(goods_data.get("goods_id"), sku_id, goods_data.get("kdtId"))
    #     submit_data = getDefaultSubmitMessage(bookKey)
    #     data = submit(bookKey, sku_id, goods_data, submit_data)
    #     if data.get("code") == 101910001:
    #         print(data)
    #         break
    for i in range(int(getConfig("grab_thread"))):
        t1 = threading.Thread(target=startThreadFun, args=(goods_data,sku_id,buy_num))
        t1.start()


def startThreadFun(goods_data, sku_id,buy_num):
    sell_out_count = 0;
    no_putaway_count = 0
    by_again_count = 0
    startTime = datetime.datetime.strptime(getConfig("start_time"), '%Y-%m-%d %H:%M:%S')
    print('等待执行:当前时间：{}，抢购时间：{}'.format(datetime.datetime.now(),startTime))

    pre_time = startTime + datetime.timedelta(minutes=-1)
    # pre_time = datetime.datetime.strptime(pre_time, '%Y-%m-%d %H:%M:%S')
    # print(,"%Y-%m-%d %H:%M:%S")
    while datetime.datetime.now() < startTime:
        time.sleep(0.01)
        if datetime.datetime.now() > pre_time:
            print('等待执行', datetime.datetime.now(), startTime)
    while True:
        if sell_out_count >= int(getConfig("sell_out_count")):

            break
        if no_putaway_count >=  int(getConfig("no_putaway_count")):

            break
        if by_again_count>= int(getConfig("by_again_count")):

            break
        bookKey = preCreateOrder(goods_data.get("goods_id"), sku_id, goods_data.get("kdtId"),buy_num)
        submit_data = getDefaultSubmitMessage(bookKey)
        data = submit(bookKey, sku_id, goods_data, submit_data,buy_num)
        #销售完毕
        if data.get("code") == 101910001:
            sell_out_count +=1
            time.sleep(float(getConfig("sell_out_count_time_out")))
            continue

        #未开始售卖
        if data.get("code") == 101305009:
            time.sleep(float(getConfig("no_putaway_count_time_out")))
            print(data)
            no_putaway_count+=1
            continue

        if data.get("code") == 0:
            time.sleep(float(getConfig("by_again_count_time_out")))
            by_again_count += 1
        else:
            time.sleep(0.2)
        time.sleep(0.01)
def auth():
    headers = {
        'Content-Type': 'application/json',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Mobile Safari/537.36',
    }
    data = {
        "key": getConfig("auth"),
        "deviceId": "1"
    }
    res = requests.post("http://122.51.204.164:8080/auth", data=json.dumps(data), headers=headers)
    if (json.loads(res.text)["code"] == "200"):
        return True
    if (json.loads(res.text)["code"] == "400"):
        print("解析网站失败！")
    return False


if __name__ == '__main__':
    banner ="""
                                                                                          
                                                                                                    
                        DDD                                                                         
                       DDDDD                                                                        
                       DDDDDj                                                                       
                       DDDDDG                        it                                             
                      ;DDDDDf                       K##              ##  ##    ## ##                
                      DDDDDD:                       ##               ##  ##   G#j ##                
                      DDDDDD.                ####################    ######## ##########            
                      DDDDDD                 ####################   ##;;;##;;G##;;##;;;;            
                      DDDDDD                      ###                #   ##   #   ##                
                     DDDDDDD                     f##               K#########t##########.           
                     DDDDDDD                    :##############    E#########i##########:           
                     DDDDDDDD ,LDDDDDi         :###############       ##  ##    ## ##  #j           
                    LDDDDDDDDDDDDDDDDD        ;####          ##      .## W###E ##, ##  ##           
                    DDDDDDDDDDDDDDDDDD       ,#####          ##     ###  ##W .###  #####i           
                   DDDDDDDDDDDDDDDDDDD       ### ##############     ##.       ##                    
                   DDDDDDDDDDDDDDDDDDE       ##  ##############     G ################              
                 :DDDDDDDDDDDDDDDDDDDD           ##          ##       ################              
             .D  DDDDDDDDDDDDDDDDDDDDD           ##          ##       ###           ##              
          DDDDD  DDDDDDDDDDDDDDDDDDDDD           ##############       ###    ##     ##              
          DDDDD  DDDDDDDDDDDDDDDDDDDDD           ##############       ###   .##     ##              
          DDDDD  DDDDDDDDDDDDDDDDDDDD,           ##          ##       ###   ####    ::              
          DDDDD  DDDDDDDDDDDDDDDDDDDD            ##         ,##          #########i    .            
          DDDDD  DDDDDDDDDDDDDDDDDDDD:           ##       #####     #########  E########            
          DDDDD  DDDDDDDDDDDDDDDDDDDDj           ##       ####      ######        D#####            
          DDDDD  DDDDDDDDDDDDDDDDDDDD.                                                              
          DDDDD  DDDDDDDDDDDDDDDDDDDD                                                               
          DDDDD  DDDDDDDDDDDDDDDDDDDD                                                               
          DDDDD  DDDDDDDDDDDDDDDDDDDD        W    f #    L      , # #G W    #    # #  W #E #        
          DDDDD  DDDDDDDDDDDDDDDDDDDD        # # #       L   i    # #  #    ,       W , i  #        
          DDDDD  DDDDDDDDDDDDDDDDDDD           i #       L  #  i  # #  #            # : i  #        
          DDDDD  DDDDDDDDDDDDDDL.             #     D    # #   #  # #  #    #  D L    : i  #        
          DDDDD  DDDDDDDf                     #    ,                ,             ,                 
          DDDDD  E;                          j                                                      
          iL                                                 
    
    """
    print(banner)
    config = configparser.RawConfigParser()
    config.read("youzan-grab.ini", encoding="utf-8")
    if(auth() == True):
        cookie = getConfig("cookie")
        headers = {
            'Content-Type': 'application/json',
            'User-Agent': 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Mobile Safari/537.36',
            'Cookie': cookie,
            # 'Host': 'cashier.youzan.com',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        }

        main()
    else:
        print("参数或者网站规则变化！请联系管理员:3337128965")