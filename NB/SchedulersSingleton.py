from apscheduler.schedulers.blocking import BlockingScheduler
#开启定时任务
class SchedulersSingleton(object):
    def get_schedulers(self):
        scheduler = BlockingScheduler()
        return scheduler

schedulers_singleton = SchedulersSingleton()