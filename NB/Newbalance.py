import json
import os
import time,datetime

import requests

from NB.JsonDataParse import parseJsonFile
from NB.ParseIni import parse_ini
from NB.SchedulersSingleton import schedulers_singleton



class NBApplication(object):
    def __init__(self,sessionkey,product_id,size):
        self.headers = {
            'Content-Type': 'application/json',
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36 MicroMessenger/7.0.9.501 NetType/WIFI MiniProgramEnv/Windows WindowsWechat',
        }
        self.sessionkey = sessionkey
        self.product_id =product_id
        self.size = size

    #post请求
    def post(self,url: object, data: object) -> object:
        res = requests.post(url, headers=self.headers, data=data)
        textToJson = res.json()
        print("当前请求的url为：{} 状态：{}".format(url, textToJson["code"]))
        if textToJson["code"] == 9001 or textToJson["code"] == 8001:
            print(textToJson["message"])
        return textToJson

    # 第一步 找去用户的地址
    def queryDefaultAddress(self):
        url = "https://crm.newbalance.com.cn/mall/address/queryDefaultAddress"
        data = {
            "sessionkey": self.sessionkey
        }
        res = self.post(url, json.dumps(data))
        success = res.get("success")
        if success:
            return res.get("data")
        return None

    #提交订单
    def orderSave(self,address_id,product_id):
        url = "https://crm.newbalance.com.cn/mall/order/save"
        data = {
            "product_id": product_id,
            "qty": 1,
            "address_id":address_id,
            "type": "1",
            "sessionkey": self.sessionkey
        }

        res = self.post(url, json.dumps(data))
        print(res)
        return res;

    #商品详情输出
    def productInfo(self):
        url = "https://crm.newbalance.com.cn/mall/center/productInfo";
        data = {
            "product_id": self.product_id,
            "sessionkey": self.sessionkey
        }
        try:
            res = self.post(url, json.dumps(data))
            sizeList = res.get("data").get("detail")[0].get("sizeList")
            print("当前sku信息如下：\n")
            for sku in sizeList:
                # print(sku)
                print("颜色：{},尺码：{},规格值：{} ,库存：{}".format(sku.get("color"), sku.get("size"), sku.get("product_id"),
                                                         sku.get("stock")))
            return res
        except AttributeError:
            print("错误！")
            return None;

    #发送钉钉消息
    def sendDing(self,message):
        url = "https://oapi.dingtalk.com/robot/send?access_token=dd6b8d21a46184a5d982d4e371cdf1a631b99b0d5368ad28cb355053577cd62b"
        headers = {
            'Content-Type': 'application/json',
            'User-Agent': 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Mobile Safari/537.36',
        }

        data = {
            "msgtype": "text",
            "text": {
                "content": "Newbalance通知：抢购成功！内容:{}".format(message)
            },
            "at": {
                "atMobiles": [
                    "15519447512",
                    "15085476476"
                ],
                "isAtAll": False
            }
        }

        send = requests.post(url, headers=headers, data=json.dumps(data))
        print(send.text)

    def checkSize(self,sizeList,size):
        for sku in sizeList:
            if sku.get("size") == size:
                if float(sku.get("stock")) > 0.0:
                    return sku.get("product_id")
        return None
def start(sessionkey,product_id,size):

    print("开始执行任务：当前时间：{}".format(datetime.datetime.now()))
    # 获取地址
    nb_application = NBApplication(sessionkey,product_id,size)
    address = nb_application.queryDefaultAddress()
    address_id = address.get("address_id");
    stock_count =0
    submit_count =0
    sku_id = '0'
    try_again_count = int(parse_ini.getConfig("stock_time_count"))
    submit_time_count = int(parse_ini.getConfig("submit_time_count"))
    if address_id != None :


        while True:
            if try_again_count < stock_count:
                print("当前已达到最大尝试次数！次数：{}".format(try_again_count));
                break
            print("抢购尺码：{}，正常尝试提交!当前第{}次".format(size,stock_count))
            productInfoRes = nb_application.productInfo()
            sizeList = productInfoRes.get("data").get("detail")[0].get("sizeList")
            #检查尺码是否存在
            prid = nb_application.checkSize(sizeList,size)
            if prid != None:
                sku_id = prid
                break
            #不存在、继续检查
            time.sleep(int(parse_ini.getConfig("stock_time")))
            stock_count +=1;
        while True:
            if submit_count>submit_time_count:
                print("抢购失败！")
                break
            if sku_id == '0':
                print("抢购失败！")
                break
            res = nb_application.orderSave(address_id,sku_id)

            if res.get("code") == 9010:
                time.sleep(int(parse_ini.getConfig("submit_time")))
            if res.get("code") == 8001:
                print("超时了！等待恢复吧。或者换个账号试试")
                time.sleep(10)
                break
            if res.get("code") == 1000:
                print("抢购成功！")

                msg = "尺码：{} 地址：{}".format(size,str(address))


                nb_application.sendDing(msg)
                print("尺码：{}，抢购成功！已发送消息到钉钉".format(size))
                break
            time.sleep(int(parse_ini.getConfig("submit_time")))
            submit_count+1
    else:
        print("请填写默认地址！")


if __name__ == '__main__':
    banner= """
                                          
                    DLLLLL   tLLLLLLLGG           
                    LGGGGG   LGGGLLLGLLG          
                                ,LLLGGGGi         
                   LGLGGGLD GGGGGj  .LGG;         
                  DGGGGGGLLLGGGGL   .GGL          
                           fGGGL.   LGLt          
                    ,fGGGLLLGGGLLLLGLGt           
                 LGGLLGGGGGGGGGLGLGGG             
                    .,GGGLLGLGG   GGGL            
                    :iLDGLLLLf    LGGGG           
               DLGLLGLLGGGGGGL   .LGGLE           
               DGGLLG.GGGGGGLLGGLGGGGG            
                       .  iLGGGGGGGLLD            
              GGGGLL   GGGGGLLLLLGLGt             
             GLLLLLD   GLLLLLLLLLLi               
                   .                              
                   G         G                    
   GLG  DL D  G D  fGL  :Gif G GG:jD;G  :Gf  DG   
   L L,Df.G G.L G  G .G L.jG GGL LfGD:D:L LELj f  
   L .GLGGL:LLG L  G  Li.  G GL.  fL  LL    LLGL  
   G .GG  , GL f   L  L f  G GD  DfG  LLD tfL: ,  
   L .G LGG  L G   EGL  GGGL L LG,LL. G DGL  GG;                            
    
    """
    print(banner)


    product_id = parse_ini.getConfig("product_id")
    jsondata = parseJsonFile()
    scheduler = schedulers_singleton.get_schedulers()
    for data in jsondata:
        sessionKey = data.get("sessionKey")
        sizes = data.get("size")
        print(sessionKey)
        for size in sizes:
            print(size)
            scheduler.add_job(start, 'cron', hour=14, minute=00, second=31, args=[sessionKey, product_id, str(size)])
    print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C    '))

    try:
        scheduler.start()
    except (KeyboardInterrupt, SystemExit):
        pass