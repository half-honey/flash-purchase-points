# 解析配置文件
import configparser


class ParseIni:
    def __init__(self):
        self.config = configparser.RawConfigParser()
        self.config.read("Newbalance.ini", encoding="utf-8")

    def getConfig(self, key):
        r = self.config.get("Basic", key)
        return r


parse_ini = ParseIni()
